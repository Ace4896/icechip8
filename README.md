# IceCHIP8

A CHIP-8 interpreter written in Rust.

![IceCHIP8](./docs/IceCHIP8.gif)

## Requirements

Common requirements:

- Stable Rust
  - This was tested on 1.42.0, but may work on older versions.

To compile for web, the following is also needed:

- The `wasm32-unknown-unknown` target (run `rustup target add wasm32-unknown-unknown`)
- [`wasm-bindgen-cli`](https://crates.io/crates/wasm-bindgen-cli)

On Linux, see:

- [List of Main Dependencies](https://github.com/hecrj/iced/issues/256)
- [Window opens, but doesn't show anything due to missing default config for `fontconfig`](https://github.com/hecrj/iced/issues/199)

## Usage

### Control Mapping

The CHIP-8 has the following keyboard layout:

```
-----------------
| 1 | 2 | 3 | C |
-----------------
| 4 | 5 | 6 | D |
-----------------
| 7 | 8 | 9 | E |
-----------------
| A | 0 | B | F |
-----------------
```

This maps to:

```
-----------------
| 1 | 2 | 3 | 4 |
-----------------
| Q | W | E | R |
-----------------
| A | S | D | F |
-----------------
| Z | X | C | V |
-----------------
```

To see the controls for each game, see [`docs/game_docs/GAMES.TXT`](docs/game_docs/GAMES.TXT).

### Compiling for Web

**NOTE:** Iced currently doesn't support the canvas, event subscriptions and time-based subscriptions on Web yet. Once these are implemented, this should work on Web!

Iced on Web requires different instructions to get up and running. These instructions were adapted from the [`iced_web` README](https://github.com/hecrj/iced/tree/master/web).

First, run these commands to compile the WASM application itself (**NOTE**: these instructions are for a debug build):

```
cargo build --target wasm32-unknown-unknown
wasm-bindgen target/wasm32-unknown-unknown/debug/icechip8.wasm --out-dir icechip8 --web
```

Then, inside the `icechip8` folder that was created, create an `index.html` file to load the WASM application:

```html
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>IceCHIP8</title>
    </head>
    <body>
        <script type="module">
            import init from "./icechip8.js";
            init('./icechip8_bg.wasm');
        </script>
    </body>
</html>
```

You can then run a web server that hosts the `icechip8` directory.

## Credits

- Bundled ROMs: [David Winter's CHIP-8 Page](http://pong-story.com/chip8/)
- `test_opcode` ROM by [corax89](https://github.com/corax89/chip8-test-rom)
- `BC_test` ROM by BestCoder (see [`docs/BC_test.txt`](./docs/BC_test.txt))
- Technical Documentation Sources:
  - [Cowgod's CHIP-8 Technical Reference](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)
  - [Mastering CHIP-8](http://mattmik.com/files/chip8/mastering/chip8.html)
- Other Useful Resources:
  - [StackExchange: How are held down keys handled in CHIP-8?](https://retrocomputing.stackexchange.com/questions/358/how-are-held-down-keys-handled-in-chip-8)
    - `Fx0A` was one instruction that I found ambiguous to implement. I've used an approach similar to what was described here, but the key is registered upon pressing it (rather than releasing it).
