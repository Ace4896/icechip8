//! # Main Menu Page
//!
//! Contains the view for the main menu page.

use iced::{button, Align, Button, Column, Element, HorizontalAlignment, Text, VerticalAlignment};

use super::{Message, Page};

fn main_menu_button<'a>(state: &'a mut button::State, label: &str) -> Button<'a, Message> {
    Button::new(
        state,
        Text::new(label)
            .horizontal_alignment(HorizontalAlignment::Center)
            .vertical_alignment(VerticalAlignment::Center),
    )
    .min_width(250)
    .min_height(40)
}

pub fn main_menu<'a>(
    load_bundled_state: &'a mut button::State,
    settings_button_state: &'a mut button::State,
) -> Element<'a, Message> {
    let title = Text::new("IceCHIP8")
        .size(32)
        .horizontal_alignment(HorizontalAlignment::Center)
        .vertical_alignment(VerticalAlignment::Center);

    let load_bundled_button = main_menu_button(load_bundled_state, "Load Bundled ROM")
        .on_press(Message::Navigation(Page::BundledRoms));
    let settings_button = main_menu_button(settings_button_state, "Settings")
        .on_press(Message::Navigation(Page::Settings));

    let buttons_column =
        Column::with_children(vec![load_bundled_button.into(), settings_button.into()])
            .align_items(Align::Center)
            .spacing(10);

    Column::with_children(vec![title.into(), buttons_column.into()])
        .align_items(Align::Center)
        .spacing(100)
        .into()
}
