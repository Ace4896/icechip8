//! # Emulation Page
//!
//! Contains the view for the main emulation page, as well as the logic for drawing the CHIP-8's display state to the screen.

use iced::{
    button,
    canvas::{Canvas, Cursor, Fill, Geometry, Program},
    Align, Button, Color, Column, Element, HorizontalAlignment, Length, Point, Rectangle, Size,
    Text, VerticalAlignment,
};

use crate::chip8;

use super::{Chip8State, Message};

/// Gets the bounds to use for rendering the display, based on the bounds of the rectangle.
///
/// This calculates the largest integer scale available for the Rectangle's boundaries,
/// returning the new boundaries and the scale to use.
fn get_display_bounds_and_scale(actual_bounds: Rectangle) -> (Rectangle, f32) {
    // Find the largest possible size which respects integer scaling
    let horizontal_scale = actual_bounds.width / chip8::DISP_WIDTH as f32;
    let vertical_scale = actual_bounds.height / chip8::DISP_HEIGHT as f32;
    let min_scale = horizontal_scale.min(vertical_scale).trunc();

    let width = chip8::DISP_WIDTH as f32 * min_scale;
    let height = chip8::DISP_HEIGHT as f32 * min_scale;

    // Determine the new top-left coordinate (as we're shrinking the usable area)
    let x = actual_bounds.x + ((actual_bounds.width - width) / 2.0);
    let y = actual_bounds.y + ((actual_bounds.height - height) / 2.0);
    let new_rect = Rectangle::new(Point::new(x, y), Size::new(width, height));

    (new_rect, min_scale)
}

impl Program<Message> for Chip8State {
    fn draw(&self, bounds: Rectangle, _cursor: Cursor) -> Vec<Geometry> {
        let (bounds, scale) = get_display_bounds_and_scale(bounds);
        let pixel_size = Size::new(scale, scale);

        let background = self.background_cache.draw(bounds.size(), |frame| {
            frame.fill_rectangle(
                Point::new(0.0, 0.0),
                bounds.size(),
                Fill::from(Color::BLACK),
            );
        });

        let display = self.display_cache.draw(bounds.size(), |frame| {
            // Go through the current display state and draw pixels where appropriate
            let display_state = self.chip8.get_display_state();
            for x in 0..chip8::DISP_WIDTH {
                for y in 0..chip8::DISP_HEIGHT {
                    if display_state[x][y] {
                        let top_left = Point::new(x as f32 * scale, y as f32 * scale);

                        frame.fill_rectangle(top_left, pixel_size, Fill::from(Color::WHITE));
                    }
                }
            }
        });

        // Convert this frame into geometry
        vec![background, display]
    }
}

pub fn emulation<'a>(
    state: &'a mut Chip8State,
    stop_button_state: &'a mut button::State,
) -> Element<'a, Message> {
    let stop_button = Button::new(
        stop_button_state,
        Text::new("Stop")
            .horizontal_alignment(HorizontalAlignment::Center)
            .vertical_alignment(VerticalAlignment::Center),
    )
    .on_press(Message::StopEmulation)
    .min_width(200)
    .min_height(40);

    let canvas = Canvas::new(state).width(Length::Fill).height(Length::Fill);

    Column::with_children(vec![stop_button.into(), canvas.into()])
        .align_items(Align::Center)
        .spacing(20)
        .into()
}
