//! # Settings Page
//!
//! Contains the view for the settings page.

use iced::{
    button, slider, Align, Button, Column, Element, HorizontalAlignment, Length, Row, Slider, Text,
    VerticalAlignment,
};

use super::{Chip8Settings, Message, Page, SettingsMessage};

fn settings_button<'a>(state: &'a mut button::State, label: &str) -> Button<'a, Message> {
    Button::new(
        state,
        Text::new(label)
            .horizontal_alignment(HorizontalAlignment::Center)
            .vertical_alignment(VerticalAlignment::Center),
    )
    .min_width(250)
    .min_height(40)
}

pub fn settings<'a>(
    settings: &'a Chip8Settings,
    cpu_clock_speed_state: &'a mut slider::State,
    back_state: &'a mut button::State,
) -> Element<'a, Message> {
    let title = Text::new("Settings")
        .size(32)
        .horizontal_alignment(HorizontalAlignment::Center)
        .vertical_alignment(VerticalAlignment::Center);

    let cpu_clock_speed_label = Text::new(format!(
        "CPU Clock Speed: {} Hz",
        settings.cpu_clock_speed_hz
    ))
    .horizontal_alignment(HorizontalAlignment::Right);

    let cpu_clock_speed_slider = Slider::new(
        cpu_clock_speed_state,
        500.0..=1000.0,
        settings.cpu_clock_speed_hz as f32,
        |value| -> Message { Message::SettingsChanged(SettingsMessage::ClockSpeed(value)) },
    )
    .width(Length::Units(500));

    // TODO: These controls should be grouped as a grid (in case I add more)
    //       Then I don't need to make rows for each control in this form
    //       Waiting on https://github.com/hecrj/iced/pull/189
    let cpu_clock_speed_controls = Row::with_children(vec![
        cpu_clock_speed_label.into(),
        cpu_clock_speed_slider.into(),
    ])
    .spacing(10)
    .align_items(Align::Center);

    let back_button =
        settings_button(back_state, "Back").on_press(Message::Navigation(Page::MainMenu));

    Column::with_children(vec![
        title.into(),
        cpu_clock_speed_controls.into(),
        back_button.into(),
    ])
    .align_items(Align::Center)
    .spacing(100)
    .into()
}
