//! # Bundled ROMs Page
//!
//! Contains the view for the bundled ROMs page.

use iced::{
    button, scrollable, Align, Button, Column, Element, HorizontalAlignment, Length, Scrollable,
    Text, VerticalAlignment,
};

use super::{Message, Page};
use crate::roms::BundledRom;

pub fn bundled_roms<'a>(
    available_roms: &'a [BundledRom],
    rom_button_states: &'a mut [button::State],
    rom_button_view_state: &'a mut scrollable::State,
    back_button_state: &'a mut button::State,
) -> Element<'a, Message> {
    assert_eq!(
        available_roms.len(),
        rom_button_states.len(),
        "Error: No. of available ROMs doesn't match the number of button states provided!"
    );

    let title = Text::new("Load ROM")
        .size(32)
        .horizontal_alignment(HorizontalAlignment::Center)
        .vertical_alignment(VerticalAlignment::Center);

    let mut rom_buttons: Vec<Element<'a, Message>> = Vec::with_capacity(rom_button_states.len());
    for (bundled_rom, state) in available_roms.iter().zip(rom_button_states.iter_mut()) {
        rom_buttons.push(
            Button::new(
                state,
                Text::new(bundled_rom.name)
                    .horizontal_alignment(HorizontalAlignment::Center)
                    .vertical_alignment(VerticalAlignment::Center),
            )
            .on_press(Message::Navigation(Page::Emulation(*bundled_rom)))
            .min_width(250)
            .min_height(40)
            .into(),
        );
    }

    let rom_button_view = Scrollable::new(rom_button_view_state)
        .push(
            Column::with_children(rom_buttons)
                .align_items(Align::Center)
                .spacing(10)
                .width(Length::Fill),
        )
        .max_height(400);

    let back_button = Button::new(
        back_button_state,
        Text::new("Back")
            .horizontal_alignment(HorizontalAlignment::Center)
            .vertical_alignment(VerticalAlignment::Center),
    )
    .min_width(250)
    .min_height(40)
    .on_press(Message::Navigation(Page::MainMenu));

    Column::with_children(vec![
        title.into(),
        rom_button_view.into(),
        back_button.into(),
    ])
    .align_items(Align::Center)
    .spacing(100)
    .into()
}
