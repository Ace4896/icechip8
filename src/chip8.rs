//! # CHIP-8 Module
//!
//! This module contains the main CHIP-8 interpreter implementation.
//!
//! Everything in here can operate separately from the frontend - all the frontend needs to do is call the relevant functions (with correct timings of course).

use std::fmt;

use rand::prelude::random;

pub mod keyboard;

mod font;

#[cfg(test)]
mod tests;

/// The size of main memory.
/// We can address locations 0x000 - 0xFFF, so there are 0x1000 addresses.
pub const RAM_SIZE: usize = 0x1000;

/// The size of the stack.
const STACK_SIZE: usize = 16;

/// The number of Vx registers in the system.
/// We have registers V0 - VF, so there are 16 registers.
const VX_REGISTER_COUNT: usize = 16;

/// The value the Program Counter (PC) should be intialised to when starting the CHIP-8.
/// 0x000 - 0x1FF is reserved for the interpreter, which is why it starts here.
const PC_START: usize = 0x200;

/// Where the default font starts in RAM.
/// This is loaded in-order, i.e. 0, 1, 2, ..., A, B, ..., F
const FONT_START: usize = 0x000;

/// The width of the display.
pub const DISP_WIDTH: usize = 64;

/// The height of the display.
pub const DISP_HEIGHT: usize = 32;

/// Asserts that the opcode in the specified instruction is equal to the expected opcode, e.g.:
///
/// ```rust
/// let instruction = Instruction::from_hi_lo(0x12, 0x20);
/// assert_opcode!(instruction, 1);
/// ```
macro_rules! assert_opcode {
    ($instruction:expr, $opcode:expr) => {
        assert_eq!(
            $instruction.opcode(),
            $opcode,
            "Error: Attempted to use non-{:X} opcode for {:X} opcode instructions!",
            $opcode,
            $opcode,
        );
    };
}

/// Represents a 16-bit instruction.
/// This is used to simplify passing instructions to other functions.
#[derive(Debug)]
struct Instruction {
    instruction: u16,
}

impl Instruction {
    /// Creates a new instruction.
    /// Only intended for testing.
    #[cfg(test)]
    pub fn new(instruction: u16) -> Self {
        Self { instruction }
    }

    /// Creates a new instruction from the higher byte and the lower byte.
    pub fn from_hi_lo(hi: u8, lo: u8) -> Self {
        let hi_u16 = (hi as u16) << 8;
        let lo_u16 = lo as u16;

        Self {
            instruction: hi_u16 | lo_u16,
        }
    }

    /// Returns the raw value stored by this struct. Only intended for debugging purposes.
    pub fn raw_value(&self) -> u16 {
        self.instruction
    }

    /// Returns the opcode for this instruction, stored in the highest nibble (i.e. n in 0xn000)
    #[inline]
    pub fn opcode(&self) -> u8 {
        ((self.instruction & 0xF000) >> 12) as u8
    }

    /// Returns the lowest 3 nibbles (i.e. nnn in 0x0nnn).
    #[inline]
    pub fn nnn(&self) -> u16 {
        self.instruction & 0x0FFF
    }

    /// Returns the lowest byte (i.e. nn in 0x00nn).
    #[inline]
    pub fn nn(&self) -> u8 {
        (self.instruction & 0x00FF) as u8
    }

    /// Returns the lowest nibble (i.e. n in 0x000n).
    #[inline]
    pub fn n(&self) -> u8 {
        (self.instruction & 0x000F) as u8
    }

    /// Returns the 2nd highest nibble (i.e. x in 0x0x00)
    #[inline]
    pub fn x(&self) -> u8 {
        ((self.instruction & 0x0F00) >> 8) as u8
    }

    /// Returns the 2nd lowest nibble (i.e. y in 0x00y0)
    #[inline]
    pub fn y(&self) -> u8 {
        ((self.instruction & 0x00F0) >> 4) as u8
    }
}

/// Represents the status of the CPU with regards to waiting for keyboard presses.
#[derive(Debug, Copy, Clone)]
pub enum KeyStatus {
    NotWaiting,
    Waiting { register: usize },
    KeyPressed { register: usize, key: keyboard::Key },
}

/// Represents the state of a CHIP-8 system.
///
/// In terms of memory, this contains:
/// - Main Memory / RAM
/// - 8-Bit V0 - VF Registers
///   - V0 - VE are used for data.
///   - VF is typically used for flags set by instructions.
/// - 16-Bit I Register
///   - Typically used for storing memory addresses, so normally only the lowest 12 bits are used.
/// - 8-Bit Delay and Sound Timer Registers
///   - These are decremeted at a rate of 60hz.
/// - 16-Bit Program Counter (PC)
/// - 16-Bit Stack (16 Elements)
/// - 8-Bit Stack Pointer
///   - This stores the next location where a value can be placed on the stack.
///
/// Other things this contains:
/// - The state of the 64x32 display, where (0, 0) is the top left pixel.
/// - The state of the keyboard, and whether the processor is waiting for a key event to occur or not.
#[derive(Clone)]
pub struct Chip8 {
    // Main Memory and Registers
    ram: [u8; RAM_SIZE],
    v: [u8; VX_REGISTER_COUNT],
    i: u16,

    // Timer Registers
    delay_timer: u8,
    sound_timer: u8,

    // PC and Stack
    pc: usize,
    stack: [usize; STACK_SIZE],
    stack_ptr: usize,

    /// The current state of the display.
    /// `display[x][y]` (0-based) gives the state of the pixel x units away from the left and y units away from the top.
    display: [[bool; DISP_HEIGHT]; DISP_WIDTH],

    /// Whether or not we need to redraw the display.
    redraw_required: bool,

    /// Keyboard State
    key_states: [bool; keyboard::KEY_COUNT],

    /// Whether we're waiting on a key event. This is set to:
    /// - `None` if we're not waiting for a key
    /// - `Some(x)` if we're waiting for a key, and need to store the key value in register Vx
    waiting_on_key: KeyStatus,
}

impl Default for Chip8 {
    fn default() -> Self {
        // Load the font at the start of the interpreter reserved area
        // First, ensure there is enough space to load in the font
        assert!(FONT_START <= 0x1FF - 80);
        let mut ram: [u8; RAM_SIZE] = [0; RAM_SIZE];
        for (i, font_byte) in font::FONT.iter().enumerate() {
            ram[i + FONT_START] = *font_byte;
        }

        Self {
            ram,
            v: [0; VX_REGISTER_COUNT],
            i: 0,

            delay_timer: 0,
            sound_timer: 0,

            pc: PC_START,
            stack: [0; STACK_SIZE],
            stack_ptr: 0,

            display: [[false; DISP_HEIGHT]; DISP_WIDTH],
            redraw_required: true,

            key_states: [false; keyboard::KEY_COUNT],
            waiting_on_key: KeyStatus::NotWaiting,
        }
    }
}

impl fmt::Debug for Chip8 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Chip8").finish()
    }
}

impl Chip8 {
    /// Loads a ROM into main memory.
    ///
    /// If the data doesn't fit into RAM, then an error is returned.
    pub fn load_rom(&mut self, rom_data: &[u8]) -> Result<(), &'static str> {
        if rom_data.len() > RAM_SIZE - PC_START {
            Err("Error: ROM data is too large to fit into main memory!")
        } else {
            for (i, rom_byte) in rom_data.iter().enumerate() {
                self.ram[PC_START + i] = *rom_byte;
            }

            Ok(())
        }
    }

    /// Performs a CPU tick by fetching, decoding and executing the next instruction.
    /// This function should be called at the configured clock speed.
    ///
    /// **NOTE**: Only the PC is incremented by this function! Use [`decrement_timers`] to decrement the timers.
    ///
    /// [`decrement_timers`]: ./fn.decrement_timers.html
    pub fn tick(&mut self) {
        // Fetch the next instruction
        // Instructions are 16-bit, so we have to fetch and combine both sections
        let instruction = Instruction::from_hi_lo(self.ram[self.pc], self.ram[self.pc + 1]);

        // Decode and execute this instruction
        // The PC is incremented in instructions that don't involve jumping / subroutines etc.
        match instruction.opcode() {
            0 => self.cls_ret(&instruction),
            1 => self.jmp(&instruction),
            2 => self.call_subroutine(&instruction),
            3 => self.skip_if_equal(&instruction),
            4 => self.skip_if_not_equal(&instruction),
            5 => self.skip_if_registers_equal(&instruction),
            6 => self.load_from_value(&instruction),
            7 => self.add_value(&instruction),
            8 => self.register_to_register_ops(&instruction),
            9 => self.skip_if_registers_not_equal(&instruction),
            0xA => self.set_i(&instruction),
            0xB => self.jump_with_v0(&instruction),
            0xC => self.rnd(&instruction),
            0xD => self.draw(&instruction),
            0xE => self.skip_if_key(&instruction),
            0xF => self.f_ops(&instruction),
            _ => unreachable!(), // Opcode is always 1 byte, so this branch can't be hit (unless something goes wrong in opcode decoding...)
        };
    }

    /// Decrements the timers by 1.
    /// This function should be called at 60hz.
    ///
    /// Returns whether a sound should be played (i.e. when the sound timer reaches 0 from a positive value).
    pub fn decrement_timers(&mut self) -> bool {
        if self.delay_timer > 0 {
            self.delay_timer -= 1;
        }

        if self.sound_timer > 0 {
            self.sound_timer -= 1;

            if self.sound_timer == 0 {
                return true;
            }
        }

        false
    }

    // Convenience method for incrementing the PC by 2.
    fn increment_pc(&mut self) {
        self.pc += 2;
    }

    /// Sets the state of the specified key to true (pressed) or false (released).
    ///
    /// If the processor was waiting for a key event (0xFx0A) and the key was pressed,
    /// then if we have not already stored a key, store this key. This will be used on
    /// the next CPU tick.
    pub fn set_key_state(&mut self, key: keyboard::Key, pressed: bool) {
        match self.waiting_on_key {
            KeyStatus::NotWaiting => self.key_states[key as usize] = pressed,
            KeyStatus::Waiting { register } => {
                if pressed {
                    self.waiting_on_key = KeyStatus::KeyPressed { register, key }
                }
            }
            KeyStatus::KeyPressed { .. } => return,
        }
    }

    /// Returns the current state of the display.
    pub fn get_display_state(&self) -> &[[bool; 32]; 64] {
        &self.display
    }

    /// Returns whether a display redraw is required or not.
    pub fn is_redraw_required(&self) -> bool {
        self.redraw_required
    }

    /// Sets `redraw_required` to false. This should be done after drawing the display to prevent unnecessary redraws.
    pub fn unset_redraw_required(&mut self) {
        self.redraw_required = false;
    }

    /// Executes the CLS or RET function, depending on the value of the last byte.
    /// - 00E0 = Clear screen
    /// - 00EE = Return from subroutine
    fn cls_ret(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 0);

        match instruction.nn() {
            0xE0 => {
                for x in 0..DISP_WIDTH {
                    for y in 0..DISP_HEIGHT {
                        self.display[x][y] = false;
                    }
                }

                self.redraw_required = true;
                self.increment_pc();
            }
            0xEE => {
                assert_ne!(
                    self.stack_ptr, 0,
                    "Error: Attempted to return from subroutine, but stack is empty!"
                );
                self.stack_ptr -= 1;
                self.pc = self.stack[self.stack_ptr];
                self.increment_pc();
            }
            _ => panic!(
                "Error: Unknown instruction 0x{:04X}",
                instruction.raw_value()
            ),
        }
    }

    /// Jumps to the address specified by the lowest 3 nibbles.
    /// - 1nnn = Jump to address 0xnnn
    fn jmp(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 1);
        self.pc = instruction.nnn() as usize;
    }

    /// Calls the subroutine at the address specified by the lowest 3 nibbles.
    /// - 2nnn = Put current PC at top of stack, increment stack pointer and jump to nnn.
    fn call_subroutine(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 2);

        // Ensure that the stack isn't full
        assert_ne!(
            self.stack_ptr, STACK_SIZE,
            "Error: Stack overflow occurred whilst calling subroutine!"
        );

        self.stack[self.stack_ptr] = self.pc;
        self.stack_ptr += 1;
        self.pc = instruction.nnn() as usize;
    }

    /// Skips the next instruction if a register equals the given value.
    /// - 3xnn = Compare Vx and hexadecimal value nn; if they are equal, skip the next instruction
    fn skip_if_equal(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 3);

        let v = self.v[instruction.x() as usize];
        let nn = instruction.nn();

        if v == nn {
            self.increment_pc();
        }

        self.increment_pc();
    }

    /// Skips the next instruction if a register does not equal the given value.
    /// - 4xnn = Compare Vx and hexadecimal value nn; if they are not equal, skip the next instruction
    fn skip_if_not_equal(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 4);

        let v = self.v[instruction.x() as usize];
        let nn = instruction.nn();

        if v != nn {
            self.increment_pc()
        }

        self.increment_pc();
    }

    /// Skips the next instruction if the values of two registers are equal.
    /// - 5xy0 = Compare Vx and Vy; if they are equal, skip the next instruction
    fn skip_if_registers_equal(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 5);
        assert_eq!(
            instruction.n(),
            0,
            "Error: Unknown instruction 0x{:4X}",
            instruction.raw_value()
        );

        let vx = self.v[instruction.x() as usize];
        let vy = self.v[instruction.y() as usize];

        if vx == vy {
            self.increment_pc()
        }

        self.increment_pc();
    }

    /// Loads a given value into the specified register.
    /// - 6xnn = Set Vx = nn
    fn load_from_value(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 6);

        self.v[instruction.x() as usize] = instruction.nn();

        self.increment_pc();
    }

    /// Adds a given value to the specified register, storing the result in that register.
    /// This may overflow, so a wrapping addition is used.
    /// - 7xnn = Set register Vx = Vx + nn
    fn add_value(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 7);

        let x = instruction.x() as usize;
        self.v[x] = self.v[x].wrapping_add(instruction.nn());

        self.increment_pc();
    }

    /// Register-to-register operations.
    /// - 8xy0 = Set Vx = Vy
    /// - 8xy1 = Set Vx = Vx OR Vy
    /// - 8xy2 = Set Vx = Vx AND Vy
    /// - 8xy3 = Set Vx = Vx XOR Vy
    /// - 8xy4 = Set Vx = Vx + Vy. Set VF = 1 if result > 8 bits, VF = 0 otherwise
    /// - 8xy5 = Set VF = 1 if Vx > Vy, VF = 0 otherwise. Then set Vx = Vx - Vy
    /// - 8xy6 = Set VF = LSB of Vx, then set Vx = Vx SHR 1 (right shift by 1)
    /// - 8xy7 = Set VF = 1 if Vy > Vx, VF = 0 otherwise. Then set Vx = Vy - Vx
    /// - 8xyE = Set VF = MSB of Vx, then set Vx = Vx SHL 1 (left shift by 1)
    fn register_to_register_ops(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 8);

        let x = instruction.x() as usize;
        let y = instruction.y() as usize;

        // Determine which register-to-register operation to perform
        match instruction.n() {
            0 => self.v[x] = self.v[y],
            1 => self.v[x] = self.v[x] | self.v[y],
            2 => self.v[x] = self.v[x] & self.v[y],
            3 => self.v[x] = self.v[x] ^ self.v[y],
            4 => {
                let (sum, overflow) = self.v[x].overflowing_add(self.v[y]);
                self.v[x] = sum;
                self.v[0xF] = overflow as u8;
            }
            5 => {
                let (sub, overflow) = self.v[x].overflowing_sub(self.v[y]);
                self.v[x] = sub;
                self.v[0xF] = !overflow as u8;
            }
            6 => {
                self.v[0xF] = self.v[x] & 0x01;
                self.v[x] >>= 1;
            }
            7 => {
                let (subn, overflow) = self.v[y].overflowing_sub(self.v[x]);
                self.v[x] = subn;
                self.v[0xF] = !overflow as u8;
            }
            0xE => {
                self.v[0xF] = self.v[x] >> 7;
                self.v[x] <<= 1;
            }
            _ => panic!(
                "Error: Unknown instruction 0x{:04X}",
                instruction.raw_value()
            ),
        }

        self.increment_pc();
    }

    /// Skips the next instruction if the values of two registers are not equal.
    /// - 9xy0 = Compare Vx and Vy; if they are not equal, skip the next instruction
    fn skip_if_registers_not_equal(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 9);
        assert_eq!(
            instruction.n(),
            0,
            "Error: Unknown instruction 0x{:4X}",
            instruction.raw_value()
        );

        let vx = self.v[instruction.x() as usize];
        let vy = self.v[instruction.y() as usize];

        if vx != vy {
            self.increment_pc()
        }

        self.increment_pc();
    }

    /// Sets the I register to the specified value.
    /// - Annn = Set I = nnn
    fn set_i(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 0xA);

        self.i = instruction.nnn();

        self.increment_pc();
    }

    /// Jumps to the specified location, using the value in V0 as an offset.
    /// - Bnnn = Jump to 0xnnn + V0
    fn jump_with_v0(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 0xB);

        self.pc = (instruction.nnn() + self.v[0] as u16) as usize;
    }

    /// Generates a random byte, then ANDs this result with the specified value.
    /// - Cxnn = Set Vx = <random byte> AND nn
    fn rnd(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 0xC);

        let random_byte = random::<u8>();
        self.v[instruction.x() as usize] = random_byte & instruction.nn();

        self.increment_pc();
    }

    /// Draws an n-byte sprite starting at memory location I at (Vx, Vy). Set VF = 1 if collision occurred, 0 otherwise.
    /// - Dxyn
    /// - The sprite's pixels are XORed with what's on the screen at the moment.
    /// - A collision is said to have occurred if any pixels are set from 1 -> 0 (true -> false in this interpreter).
    /// - If the sprite will be drawn offscreen, it should wrap around to the other side.
    fn draw(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 0xD);

        // The approach to drawing sprites will be drawing them from left to right, top to bottom.
        let start_x = self.v[instruction.x() as usize] as usize;
        let start_y = self.v[instruction.y() as usize] as usize;
        let n = instruction.n() as usize;
        let i = self.i as usize;
        let mut flipped = false;

        for y_offset in 0..n {
            // This bitmask is used to obtain each bit of the 8-bit sprite number individually
            // It is shifted right by the x_offset each time we draw a pixel in this row
            let mask: u8 = 0b1000_0000;

            // The current row being drawn
            let current_row = self.ram[i + y_offset];
            let y_coord = (start_y + y_offset) % DISP_HEIGHT;

            for x_offset in 0..8 {
                let x_coord = (start_x + x_offset) % DISP_WIDTH;
                let original_pixel = self.display[x_coord][y_coord];

                // This obtains the new pixel value to use as a true (1)/false (0) value
                let new_pixel = (current_row & (mask >> x_offset)) > 0;
                self.display[x_coord][y_coord] ^= new_pixel;
                if original_pixel && !self.display[x_coord][y_coord] {
                    flipped = true;
                }
            }
        }

        self.v[0xF] = flipped as u8;
        self.redraw_required = true;
        self.increment_pc();
    }

    /// Skips the next instruction depending on the status of the specified key.
    /// - Ex9E = If key with the value stored in Vx is pressed, skip next instruction
    /// - ExA1 = If key with the value stored in Vx is not pressed, skip next instruction
    fn skip_if_key(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 0xE);

        let key = self.v[instruction.x() as usize] as usize;
        match instruction.nn() {
            0x9E => {
                if self.key_states[key] {
                    self.increment_pc();
                }
            }
            0xA1 => {
                if !self.key_states[key] {
                    self.increment_pc();
                }
            }
            _ => panic!(
                "Error: Unknown keyboard instruction 0x{:04X}",
                instruction.raw_value()
            ),
        }

        self.increment_pc();
    }

    /// Miscellaneous operations that have an opcode of 'F'.
    /// - Fx07 = Set Vx = Delay Timer Value
    /// - Fx0A = Wait for a key press, and store the value of the key in Vx
    /// - Fx15 = Delay Timer = Vx
    /// - Fx18 = Sound Timer = Vx
    /// - Fx1E = Set I = I + Vx
    /// - Fx29 = I = Location of Hexadecimal Sprite for Digit Vx - this sprite is from the font provided by the interpreter
    /// - Fx33 = Store BCD representation of Vx in memory locations I, I+1 and I+2
    /// - Fx55 = Store registers V0 - Vx in memory, starting with I = V0, I+1 = V1 etc.
    /// - Fx65 = Read memory starting from I and store into V0 - Vx. V0 = memory at I, V1 = memory at I+1 etc.
    fn f_ops(&mut self, instruction: &Instruction) {
        assert_opcode!(instruction, 0xF);

        let x = instruction.x() as usize;
        let i = self.i as usize;

        match instruction.nn() {
            0x07 => self.v[x] = self.delay_timer,
            0x0A => match self.waiting_on_key {
                // NOTE: Return is used so PC isn't incremented at the end of this function
                // If we aren't already waiting for a key press, start waiting and store which register to store the key in
                KeyStatus::NotWaiting => {
                    self.waiting_on_key = KeyStatus::Waiting { register: x };
                    return;
                }
                // If we are still waiting, continue waiting
                KeyStatus::Waiting { .. } => return,
                // If a keypress was received, store the key in the relevant register
                KeyStatus::KeyPressed { register, key } => {
                    self.v[register] = key as u8;
                    self.waiting_on_key = KeyStatus::NotWaiting;
                }
            },
            0x15 => self.delay_timer = self.v[x],
            0x18 => self.sound_timer = self.v[x],
            0x1E => self.i += self.v[x] as u16,
            0x29 => {
                // Each sprite in the font is 5 bytes long
                // So for the digit '0', we need 0x000 - 0x004
                // For '1', we need 0x005 - 0x009
                // Conveniently, we can just multiply this value by 5 to get the starting index for this sprite
                self.i = FONT_START as u16 + (self.v[x] * 5) as u16;
            }
            0x33 => {
                // BCD = Binary-coded Decimal
                // The idea is the following:
                // - Say we want to encode the number 104 in BCD
                // - We split up the place values, and store each of them in RAM
                //   - 1 in binary = 0000 0001
                //   - 0 in binary = 0000 0000
                //   - 4 in binary = 0000 0100
                // - So we set:
                //   - RAM[I]   = 0000 0001 (hundreds)
                //   - RAM[I+1] = 0000 0000 (tens)
                //   - RAM[I+2] = 0000 0100 (units)
                let val = self.v[x];
                let hundreds = val / 100;
                let tens = (val % 100) / 10;
                let units = val % 10;

                self.ram[i] = hundreds;
                self.ram[i + 1] = tens;
                self.ram[i + 2] = units;
            }
            0x55 => {
                for offset in 0..=x {
                    self.ram[i + offset] = self.v[offset];
                }
            }
            0x65 => {
                for offset in 0..=x {
                    self.v[offset] = self.ram[i + offset];
                }
            }
            _ => panic!(
                "Error: Unknown misc. instruction 0x{:04X}",
                instruction.raw_value()
            ),
        }

        self.increment_pc();
    }
}
