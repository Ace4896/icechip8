//! # ROMs Module
//!
//! This module contains pre-loaded ROMs included with the emulator.
//! These ROMs are included to make demoing the final emulator (especially on Web) easier.
//!
//! See the "Credits" in the project README for the sources of these ROMs.

static TEST_OPCODE: &'static [u8] = include_bytes!("../roms/test_opcode.ch8");
static BC_TEST: &'static [u8] = include_bytes!("../roms/BC_test.ch8");

static FIFTEEN_PUZZLE: &'static [u8] = include_bytes!("../roms/15PUZZLE");
static BLINKY: &'static [u8] = include_bytes!("../roms/BLINKY");
static BLITZ: &'static [u8] = include_bytes!("../roms/BLITZ");
static BRIX: &'static [u8] = include_bytes!("../roms/BRIX");
static CONNECT4: &'static [u8] = include_bytes!("../roms/CONNECT4");
static GUESS: &'static [u8] = include_bytes!("../roms/GUESS");
static HIDDEN: &'static [u8] = include_bytes!("../roms/HIDDEN");
static INVADERS: &'static [u8] = include_bytes!("../roms/INVADERS");
static KALEID: &'static [u8] = include_bytes!("../roms/KALEID");
static MAZE: &'static [u8] = include_bytes!("../roms/MAZE");
static MERLIN: &'static [u8] = include_bytes!("../roms/MERLIN");
static MISSILE: &'static [u8] = include_bytes!("../roms/MISSILE");
static PONG: &'static [u8] = include_bytes!("../roms/PONG");
static PONG2: &'static [u8] = include_bytes!("../roms/PONG2");
static PUZZLE: &'static [u8] = include_bytes!("../roms/PUZZLE");
static SYZYGY: &'static [u8] = include_bytes!("../roms/SYZYGY");
static TANK: &'static [u8] = include_bytes!("../roms/TANK");
static TETRIS: &'static [u8] = include_bytes!("../roms/TETRIS");
static TICTAC: &'static [u8] = include_bytes!("../roms/TICTAC");
static UFO: &'static [u8] = include_bytes!("../roms/UFO");
static VBRIX: &'static [u8] = include_bytes!("../roms/VBRIX");
static VERS: &'static [u8] = include_bytes!("../roms/VERS");
static WIPEOFF: &'static [u8] = include_bytes!("../roms/WIPEOFF");

#[derive(Debug, Copy, Clone)]
pub struct BundledRom {
    pub name: &'static str,
    pub data: &'static [u8],
}

lazy_static! {
    pub static ref BUNDLED_ROMS: Vec<BundledRom> = vec![
        BundledRom {
            name: "Test Opcode (corax89)",
            data: TEST_OPCODE
        },
        BundledRom {
            name: "BC Test (BestCoder)",
            data: BC_TEST
        },
        BundledRom {
            name: "15PUZZLE",
            data: FIFTEEN_PUZZLE
        },
        BundledRom {
            name: "BLINKY",
            data: BLINKY
        },
        BundledRom {
            name: "BLITZ",
            data: BLITZ
        },
        BundledRom {
            name: "BRIX",
            data: BRIX
        },
        BundledRom {
            name: "CONNECT4",
            data: CONNECT4
        },
        BundledRom {
            name: "GUESS",
            data: GUESS
        },
        BundledRom {
            name: "HIDDEN",
            data: HIDDEN
        },
        BundledRom {
            name: "INVADERS",
            data: INVADERS
        },
        BundledRom {
            name: "KALEID",
            data: KALEID
        },
        BundledRom {
            name: "MAZE",
            data: MAZE
        },
        BundledRom {
            name: "MERLIN",
            data: MERLIN
        },
        BundledRom {
            name: "MISSILE",
            data: MISSILE
        },
        BundledRom {
            name: "PONG",
            data: PONG
        },
        BundledRom {
            name: "PONG2",
            data: PONG2
        },
        BundledRom {
            name: "PUZZLE",
            data: PUZZLE
        },
        BundledRom {
            name: "SYZYGY",
            data: SYZYGY
        },
        BundledRom {
            name: "TANK",
            data: TANK
        },
        BundledRom {
            name: "TETRIS",
            data: TETRIS
        },
        BundledRom {
            name: "TICTAC",
            data: TICTAC
        },
        BundledRom {
            name: "UFO",
            data: UFO
        },
        BundledRom {
            name: "VBRIX",
            data: VBRIX
        },
        BundledRom {
            name: "VERS",
            data: VERS
        },
        BundledRom {
            name: "WIPEOFF",
            data: WIPEOFF
        },
    ];
}
