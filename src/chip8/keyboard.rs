//! # Keyboard Module
//!
//! Contains frontend-agnostic representations of the CHIP-8 keyboard.

/// The number of keys on the CHIP-8 keyboard.
pub const KEY_COUNT: usize = 16;

/// Represents a key that can be pressed and used by the CHIP-8.
#[repr(u8)]
#[derive(Debug, Copy, Clone)]
pub enum Key {
    K0 = 0,
    K1 = 1,
    K2 = 2,
    K3 = 3,
    K4 = 4,
    K5 = 5,
    K6 = 6,
    K7 = 7,
    K8 = 8,
    K9 = 9,
    A = 0xA,
    B = 0xB,
    C = 0xC,
    D = 0xD,
    E = 0xE,
    F = 0xF,
}
