use super::*;

mod instruction {
    use super::*;

    #[test]
    fn instruction_raw_value_is_correct() {
        let ins = Instruction::new(0x1200);
        assert_eq!(ins.raw_value(), 0x1200);
    }

    #[test]
    fn instruction_from_hi_lo_is_correct() {
        let ins1 = Instruction::from_hi_lo(0x12, 0x34);
        assert_eq!(ins1.raw_value(), 0x1234);

        let ins2 = Instruction::from_hi_lo(0x00, 0xE0);
        assert_eq!(ins2.raw_value(), 0x00E0);

        let ins3 = Instruction::from_hi_lo(0xD1, 0xA2);
        assert_eq!(ins3.raw_value(), 0xD1A2);
    }

    #[test]
    fn instruction_opcode_is_correct() {
        // Opcode = Highest Nibble
        let ins1 = Instruction::new(0x00E0);
        assert_eq!(ins1.opcode(), 0);

        let ins2 = Instruction::new(0x2AAA);
        assert_eq!(ins2.opcode(), 2);

        let ins3 = Instruction::new(0xF029);
        assert_eq!(ins3.opcode(), 0xF);
    }

    #[test]
    fn instruction_nnn_is_correct() {
        // nnn = Lowest 3 Nibbles / 12 Bits
        let ins1 = Instruction::new(0x1ABC);
        assert_eq!(ins1.nnn(), 0xABC);

        let ins2 = Instruction::new(0x2FAB);
        assert_eq!(ins2.nnn(), 0xFAB);
    }

    #[test]
    fn instruction_nn_is_correct() {
        // nn = Lowest Byte
        let ins1 = Instruction::new(0xC122);
        assert_eq!(ins1.nn(), 0x22);

        let ins2 = Instruction::new(0x7246);
        assert_eq!(ins2.nn(), 0x46);
    }

    #[test]
    fn instruction_n_is_correct() {
        // n = Lowest Nibble
        let ins1 = Instruction::new(0xD123);
        assert_eq!(ins1.n(), 3);

        let ins2 = Instruction::new(0x8447);
        assert_eq!(ins2.n(), 7);
    }

    #[test]
    fn instruction_x_is_correct() {
        // x = 2nd Highest Nibble
        let ins1 = Instruction::new(0xD123);
        assert_eq!(ins1.x(), 1);

        let ins2 = Instruction::new(0x4588);
        assert_eq!(ins2.x(), 5);

        let ins3 = Instruction::new(0x5420);
        assert_eq!(ins3.x(), 4);
    }

    #[test]
    fn instruction_y_is_correct() {
        // y = 2nd Lowest Nibble
        let ins1 = Instruction::new(0xD123);
        assert_eq!(ins1.y(), 2);

        let ins2 = Instruction::new(0x8560);
        assert_eq!(ins2.y(), 6);

        let ins3 = Instruction::new(0x5410);
        assert_eq!(ins3.y(), 1);
    }
}

#[test]
fn decrement_timers_both_non_zero_is_correct() {
    let mut chip8 = Chip8::default();
    chip8.delay_timer = 100;
    chip8.sound_timer = 50;

    let result = chip8.decrement_timers();
    assert_eq!(chip8.delay_timer, 99);
    assert_eq!(chip8.sound_timer, 49);
    assert_eq!(result, false);
}

#[test]
fn decrement_timers_delay_zero_is_correct() {
    let mut chip8 = Chip8::default();
    chip8.delay_timer = 0;
    chip8.sound_timer = 50;

    let result = chip8.decrement_timers();
    assert_eq!(chip8.delay_timer, 0);
    assert_eq!(chip8.sound_timer, 49);
    assert_eq!(result, false);
}

#[test]
fn decrement_timers_sound_already_zero_is_correct() {
    let mut chip8 = Chip8::default();
    chip8.delay_timer = 100;
    chip8.sound_timer = 0;

    let result = chip8.decrement_timers();
    assert_eq!(chip8.delay_timer, 99);
    assert_eq!(chip8.sound_timer, 0);
    assert_eq!(result, false);
}

#[test]
fn decrement_timers_delay_non_zero_sound_reaches_zero_is_correct() {
    let mut chip8 = Chip8::default();
    chip8.delay_timer = 100;
    chip8.sound_timer = 1;

    let result = chip8.decrement_timers();
    assert_eq!(chip8.delay_timer, 99);
    assert_eq!(chip8.sound_timer, 0);
    assert_eq!(result, true);
}

#[test]
fn decrement_timers_delay_zero_sound_reaches_zero_is_correct() {
    let mut chip8 = Chip8::default();
    chip8.delay_timer = 0;
    chip8.sound_timer = 1;

    let result = chip8.decrement_timers();
    assert_eq!(chip8.delay_timer, 0);
    assert_eq!(chip8.sound_timer, 0);
    assert_eq!(result, true);
}

#[test]
fn increment_pc_is_correct() {
    let mut chip8 = Chip8::default();
    chip8.pc = 0x300;

    chip8.increment_pc();
    assert_eq!(chip8.pc, 0x302);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-0 opcode for 0 opcode instructions!")]
fn cls_ret_with_non_zero_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x1234);

    chip8.cls_ret(&instruction);
}

#[test]
#[should_panic(expected = "Error: Unknown instruction 0x0123")]
fn cls_ret_with_unknown_variant_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x0123);

    chip8.cls_ret(&instruction);
}

#[test]
fn cls_ret_cls_is_correct() {
    // Setup a CHIP-8 with 3 active pixels
    let mut chip8 = Chip8::default();
    chip8.display[0][0] = true;
    chip8.display[10][5] = true;
    chip8.display[20][3] = true;

    let instruction = Instruction::new(0x00E0);
    chip8.cls_ret(&instruction);

    for x in 0..DISP_WIDTH {
        for y in 0..DISP_HEIGHT {
            assert_eq!(chip8.display[x][y], false);
        }
    }
}

#[test]
#[should_panic(expected = "Error: Attempted to return from subroutine, but stack is empty!")]
fn cls_ret_ret_with_empty_stack_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00EE);

    chip8.cls_ret(&instruction);
}

#[test]
fn cls_ret_with_address_in_stack_is_correct() {
    // Setup a CHIP-8 with PC = 0x500 and 2 things on stack:
    // - 0x200
    // - 0x312
    let mut chip8 = Chip8::default();
    chip8.pc = 0x500;
    chip8.stack[0] = 0x200;
    chip8.stack[1] = 0x312;
    chip8.stack_ptr = 2;

    let instruction = Instruction::new(0x00EE);
    chip8.cls_ret(&instruction);

    // NOTE: What happens to the location where 0x312 was shouldn't matter
    // However, do ensure that we've incremented from this instruction
    assert_eq!(chip8.pc, 0x314);
    assert_eq!(chip8.stack_ptr, 1);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-1 opcode for 1 opcode instructions!")]
fn jmp_with_non_one_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x0000);

    chip8.jmp(&instruction);
}

#[test]
fn jmp_is_correct() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x1234);

    chip8.jmp(&instruction);
    assert_eq!(chip8.pc, 0x234);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-2 opcode for 2 opcode instructions!")]
fn call_subroutine_with_non_two_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00E0);

    chip8.call_subroutine(&instruction);
}

#[test]
#[should_panic(expected = "Error: Stack overflow occurred whilst calling subroutine!")]
fn call_subroutine_with_full_stack_panics() {
    let mut chip8 = Chip8::default();
    chip8.stack_ptr = STACK_SIZE;

    let instruction = Instruction::new(0x2200);
    chip8.call_subroutine(&instruction);
}

#[test]
fn call_subroutine_is_correct() {
    // Setup a CHIP-8 state with 5 addresses on the stack (in positions 0-4) and PC of 0x400
    let mut chip8 = Chip8::default();
    chip8.pc = 0x400;
    chip8.stack_ptr = 5;

    let instruction = Instruction::new(0x2456);

    chip8.call_subroutine(&instruction);

    assert_eq!(chip8.stack_ptr, 6);
    assert_eq!(chip8.stack[5], 0x400);
    assert_eq!(chip8.pc, 0x456);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-3 opcode for 3 opcode instructions!")]
fn skip_if_equal_with_non_three_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00E0);

    chip8.skip_if_equal(&instruction);
}

#[test]
fn skip_if_equal_with_equal_values_is_correct() {
    // Setup a CHIP-8 state with a PC of 0x400
    // Set V5 = nn = 0x12
    let mut chip8 = Chip8::default();
    chip8.pc = 0x400;
    chip8.v[5] = 0x12;

    let instruction = Instruction::new(0x3512);

    chip8.skip_if_equal(&instruction);

    assert_eq!(chip8.pc, 0x404);
}

#[test]
fn skip_if_equal_with_unequal_values_is_correct() {
    // Setup a CHIP-8 state with a PC of 0x400
    // Set V5 = 0x10, nn = 0x12
    let mut chip8 = Chip8::default();
    chip8.pc = 0x400;
    chip8.v[5] = 0x10;

    let instruction = Instruction::new(0x3512);

    chip8.skip_if_equal(&instruction);

    assert_eq!(chip8.pc, 0x402);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-4 opcode for 4 opcode instructions!")]
fn skip_if_not_equal_with_non_four_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00E0);

    chip8.skip_if_not_equal(&instruction);
}

#[test]
fn skip_if_not_equal_with_unequal_values_is_correct() {
    // Setup a CHIP-8 state with a PC of 0x400
    // Set V5 = 0x10, nn = 0x12
    let mut chip8 = Chip8::default();
    chip8.pc = 0x400;
    chip8.v[5] = 0x10;

    let instruction = Instruction::new(0x4512);

    chip8.skip_if_not_equal(&instruction);

    assert_eq!(chip8.pc, 0x404);
}

#[test]
fn skip_if_not_equal_with_equal_values_is_correct() {
    // Setup a CHIP-8 state with a PC of 0x400
    // Set V5 = nn = 0x12
    let mut chip8 = Chip8::default();
    chip8.pc = 0x400;
    chip8.v[5] = 0x12;

    let instruction = Instruction::new(0x4512);

    chip8.skip_if_not_equal(&instruction);

    assert_eq!(chip8.pc, 0x402);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-5 opcode for 5 opcode instructions!")]
fn skip_if_registers_equal_with_non_five_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00E0);

    chip8.skip_if_registers_equal(&instruction);
}

#[test]
#[should_panic(expected = "Error: Unknown instruction 0x5125")]
fn skip_if_registers_equal_with_non_zero_ending_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x5125);

    chip8.skip_if_registers_equal(&instruction);
}

#[test]
fn skip_if_registers_equal_with_equal_values_is_correct() {
    // Setup a CHIP-8 state with:
    // - PC = 0x400
    // - V0 = 10
    // - V1 = 10
    let mut chip8 = Chip8::default();
    chip8.pc = 0x400;
    chip8.v[0] = 10;
    chip8.v[1] = 10;

    let instruction = Instruction::new(0x5010);

    chip8.skip_if_registers_equal(&instruction);

    assert_eq!(chip8.pc, 0x404);
}

#[test]
fn skip_if_registers_equal_with_unequal_values_is_correct() {
    // Setup a CHIP-8 state with:
    // - PC = 0x400
    // - V0 = 10
    // - V1 = 12
    let mut chip8 = Chip8::default();
    chip8.pc = 0x400;
    chip8.v[0] = 10;
    chip8.v[1] = 12;

    let instruction = Instruction::new(0x5010);

    chip8.skip_if_registers_equal(&instruction);

    assert_eq!(chip8.pc, 0x402);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-6 opcode for 6 opcode instructions!")]
fn load_from_value_with_non_six_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00E0);

    chip8.load_from_value(&instruction);
}

#[test]
fn load_from_value_is_correct() {
    // Load 0x50 into V4
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x6450);

    chip8.load_from_value(&instruction);

    assert_eq!(chip8.v[4], 0x50);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-7 opcode for 7 opcode instructions!")]
fn add_value_with_non_seven_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00E0);

    chip8.add_value(&instruction);
}

#[test]
fn add_value_with_no_overflow_is_correct() {
    // Setup a CHIP-8 state with V5 = 0x10, and add 0x20 to it
    let mut chip8 = Chip8::default();
    chip8.v[5] = 0x10;

    let instruction = Instruction::new(0x7520);

    chip8.add_value(&instruction);

    assert_eq!(chip8.v[5], 0x30);
}

#[test]
fn add_value_with_overflow_is_correct() {
    // Setup a CHIP-8 state with V5 = 0xF0, and add 0x15 to it
    // This would've given us 0x105, but since there is overflow, it should be 0x05
    let mut chip8 = Chip8::default();
    chip8.v[5] = 0xF0;

    let instruction = Instruction::new(0x7515);

    chip8.add_value(&instruction);

    assert_eq!(chip8.v[5], 0x05);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-8 opcode for 8 opcode instructions!")]
fn register_to_register_ops_with_non_eight_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00E0);

    chip8.register_to_register_ops(&instruction);
}

#[test]
fn register_to_register_ops_load_from_register_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 10
    // - V2 = 20
    let mut chip8 = Chip8::default();
    chip8.v[1] = 10;
    chip8.v[2] = 20;

    let instruction = Instruction::new(0x8120);
    chip8.register_to_register_ops(&instruction);

    assert_eq!(chip8.v[1], 20);
}

#[test]
fn register_to_register_ops_bitwise_or_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 0b1000_1100
    // - V2 = 0b0111_0111
    // So the final result should be 0b1111_1111
    let mut chip8 = Chip8::default();
    chip8.v[1] = 0b1000_1100;
    chip8.v[2] = 0b0111_0111;

    let instruction = Instruction::new(0x8121);
    chip8.register_to_register_ops(&instruction);

    assert_eq!(chip8.v[1], 0b1111_1111);
}

#[test]
fn register_to_register_ops_bitwise_and_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 0b1000_1100
    // - V2 = 0b0111_0111
    // So the final result should be 0b0000_0100
    let mut chip8 = Chip8::default();
    chip8.v[1] = 0b1000_1100;
    chip8.v[2] = 0b0111_0111;

    let instruction = Instruction::new(0x8122);
    chip8.register_to_register_ops(&instruction);

    assert_eq!(chip8.v[1], 0b0000_0100);
}

#[test]
fn register_to_register_ops_bitwise_xor_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 0b1000_1100
    // - V2 = 0b0111_0111
    // So the final result should be 0b1111_1011
    let mut chip8 = Chip8::default();
    chip8.v[1] = 0b1000_1100;
    chip8.v[2] = 0b0111_0111;

    let instruction = Instruction::new(0x8123);
    chip8.register_to_register_ops(&instruction);

    assert_eq!(chip8.v[1], 0b1111_1011);
}

#[test]
fn register_to_register_ops_add_no_overflow_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 10
    // - V2 = 20
    // So after this instruction:
    // - V1 = 30
    // - VF = 0
    let mut chip8 = Chip8::default();
    chip8.v[1] = 10;
    chip8.v[2] = 20;

    let instruction = Instruction::new(0x8124);
    chip8.register_to_register_ops(&instruction);

    assert_eq!(chip8.v[1], 30);
    assert_eq!(chip8.v[0xF], 0);
}

#[test]
fn register_to_register_ops_add_with_overflow_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 250
    // - V2 = 10
    // So after this instruction:
    // - V1 = 4 (256 would've been 0)
    // - VF = 1
    let mut chip8 = Chip8::default();
    chip8.v[1] = 250;
    chip8.v[2] = 10;

    let instruction = Instruction::new(0x8124);
    chip8.register_to_register_ops(&instruction);

    assert_eq!(chip8.v[1], 4);
    assert_eq!(chip8.v[0xF], 1);
}

#[test]
fn register_to_register_ops_sub_no_overflow_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 20
    // - V2 = 10
    // So after this instruction:
    // - V1 = 10
    // - VF = 1 (since V1 > V2)
    let mut chip8 = Chip8::default();
    chip8.v[1] = 20;
    chip8.v[2] = 10;

    let instruction = Instruction::new(0x8125);
    chip8.register_to_register_ops(&instruction);

    assert_eq!(chip8.v[1], 10);
    assert_eq!(chip8.v[0xF], 1);
}

#[test]
fn register_to_register_ops_sub_with_overflow_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 10
    // - V2 = 20
    // So after this instruction:
    // - V1 = 246
    // - VF = 0 (since V2 > V1)
    let mut chip8 = Chip8::default();
    chip8.v[1] = 10;
    chip8.v[2] = 20;

    let instruction = Instruction::new(0x8125);
    chip8.register_to_register_ops(&instruction);

    assert_eq!(chip8.v[1], 246);
    assert_eq!(chip8.v[0xF], 0);
}

#[test]
fn register_to_register_ops_right_shift_0_lsb_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 0b1111_1110
    // So after this instruction:
    // - V1 = 0b0111_1111
    // - VF = 0
    let mut chip8 = Chip8::default();
    chip8.v[1] = 0b1111_1110;

    let instruction = Instruction::new(0x8126);
    chip8.register_to_register_ops(&instruction);

    assert_eq!(chip8.v[1], 0b0111_1111);
    assert_eq!(chip8.v[0xF], 0);
}

#[test]
fn register_to_register_ops_right_shift_1_lsb_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 0b1111_1101
    // So after this instruction:
    // - V1 = 0b0111_1110
    // - VF = 1
    let mut chip8 = Chip8::default();
    chip8.v[1] = 0b1111_1101;

    let instruction = Instruction::new(0x8126);
    chip8.register_to_register_ops(&instruction);

    assert_eq!(chip8.v[1], 0b0111_1110);
    assert_eq!(chip8.v[0xF], 1);
}

#[test]
fn register_to_register_ops_reverse_sub_no_overflow_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 10
    // - V2 = 30
    // So after this instruction:
    // - V1 = 20
    // - VF = 1 (since V2 > V1)
    let mut chip8 = Chip8::default();
    chip8.v[1] = 10;
    chip8.v[2] = 30;

    let instruction = Instruction::new(0x8127);
    chip8.register_to_register_ops(&instruction);

    assert_eq!(chip8.v[1], 20);
    assert_eq!(chip8.v[0xF], 1);
}

#[test]
fn register_to_register_ops_reverse_sub_with_overflow_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 10
    // - V2 = 5
    // So after this instruction:
    // - V1 = 251
    // - VF = 0 (since V1 > V2)
    let mut chip8 = Chip8::default();
    chip8.v[1] = 10;
    chip8.v[2] = 5;

    let instruction = Instruction::new(0x8127);
    chip8.register_to_register_ops(&instruction);

    assert_eq!(chip8.v[1], 251);
    assert_eq!(chip8.v[0xF], 0);
}

#[test]
fn register_to_register_ops_left_shift_0_msb_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 0b0001_1101
    // So after this instruction:
    // - V1 = 0b0011_1010
    // - VF = 1
    let mut chip8 = Chip8::default();
    chip8.v[1] = 0b0001_1101;

    let instruction = Instruction::new(0x812E);
    chip8.register_to_register_ops(&instruction);

    assert_eq!(chip8.v[1], 0b0011_1010);
    assert_eq!(chip8.v[0xF], 0);
}

#[test]
fn register_to_register_ops_left_shift_1_msb_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 0b1011_1101
    // So after this instruction:
    // - V1 = 0b0111_1010
    // - VF = 1
    let mut chip8 = Chip8::default();
    chip8.v[1] = 0b1011_1101;

    let instruction = Instruction::new(0x812E);
    chip8.register_to_register_ops(&instruction);

    assert_eq!(chip8.v[1], 0b0111_1010);
    assert_eq!(chip8.v[0xF], 1);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-9 opcode for 9 opcode instructions!")]
fn skip_if_registers_not_equal_with_non_nine_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00E0);

    chip8.skip_if_registers_not_equal(&instruction);
}

#[test]
#[should_panic(expected = "Error: Unknown instruction 0x9125")]
fn skip_if_registers_not_equal_with_non_zero_ending_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x9125);

    chip8.skip_if_registers_not_equal(&instruction);
}

#[test]
fn skip_if_registers_not_equal_with_equal_values_is_correct() {
    // Setup a CHIP-8 state with:
    // - PC = 0x400
    // - V0 = 10
    // - V1 = 10
    let mut chip8 = Chip8::default();
    chip8.pc = 0x400;
    chip8.v[0] = 10;
    chip8.v[1] = 10;

    let instruction = Instruction::new(0x9010);

    chip8.skip_if_registers_not_equal(&instruction);

    assert_eq!(chip8.pc, 0x402);
}

#[test]
fn skip_if_registers_not_equal_with_unequal_values_is_correct() {
    // Setup a CHIP-8 state with:
    // - PC = 0x400
    // - V0 = 10
    // - V1 = 12
    let mut chip8 = Chip8::default();
    chip8.pc = 0x400;
    chip8.v[0] = 10;
    chip8.v[1] = 12;

    let instruction = Instruction::new(0x9010);

    chip8.skip_if_registers_not_equal(&instruction);

    assert_eq!(chip8.pc, 0x404);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-A opcode for A opcode instructions!")]
fn set_i_with_non_a_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00E0);

    chip8.set_i(&instruction);
}

#[test]
fn set_i_is_correct() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0xA123);

    chip8.set_i(&instruction);

    assert_eq!(chip8.i, 0x123);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-B opcode for B opcode instructions!")]
fn jump_with_v0_with_non_b_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00E0);

    chip8.jump_with_v0(&instruction);
}

#[test]
fn jump_with_v0_is_correct() {
    // Setup a CHIP-8 state with V0 = 5
    // We add this to 0x123 to get 0x128
    let mut chip8 = Chip8::default();
    chip8.v[0] = 5;

    let instruction = Instruction::new(0xB123);
    chip8.jump_with_v0(&instruction);

    assert_eq!(chip8.pc, 0x128);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-C opcode for C opcode instructions!")]
fn rnd_with_non_c_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00E0);

    chip8.rnd(&instruction);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-D opcode for D opcode instructions!")]
fn draw_with_non_d_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00E0);

    chip8.draw(&instruction);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-E opcode for E opcode instructions!")]
fn skip_if_key_with_non_e_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00E0);

    chip8.skip_if_key(&instruction);
}

#[test]
fn skip_if_key_pressed_with_pressed_key_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 0xA
    // - PC = 0x400
    // - Key A is pressed
    let mut chip8 = Chip8::default();
    chip8.v[1] = 0xA;
    chip8.pc = 0x400;
    chip8.key_states[0xA] = true;

    let instruction = Instruction::new(0xE19E);
    chip8.skip_if_key(&instruction);

    assert_eq!(chip8.pc, 0x404);
}

#[test]
fn skip_if_key_pressed_with_unpressed_key_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 0xA
    // - PC = 0x400
    // - Key A is unpressed
    let mut chip8 = Chip8::default();
    chip8.v[1] = 0xA;
    chip8.pc = 0x400;
    chip8.key_states[0xA] = false;

    let instruction = Instruction::new(0xE19E);
    chip8.skip_if_key(&instruction);

    assert_eq!(chip8.pc, 0x402);
}

#[test]
fn skip_if_key_not_pressed_with_pressed_key_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 0xA
    // - PC = 0x400
    // - Key A is pressed
    let mut chip8 = Chip8::default();
    chip8.v[1] = 0xA;
    chip8.pc = 0x400;
    chip8.key_states[0xA] = true;

    let instruction = Instruction::new(0xE1A1);
    chip8.skip_if_key(&instruction);

    assert_eq!(chip8.pc, 0x402);
}

#[test]
fn skip_if_key_not_pressed_with_unpressed_key_is_correct() {
    // Setup a CHIP-8 state with:
    // - V1 = 0xA
    // - PC = 0x400
    // - Key A is unpressed
    let mut chip8 = Chip8::default();
    chip8.v[1] = 0xA;
    chip8.pc = 0x400;
    chip8.key_states[0xA] = false;

    let instruction = Instruction::new(0xE1A1);
    chip8.skip_if_key(&instruction);

    assert_eq!(chip8.pc, 0x404);
}

#[test]
#[should_panic(expected = "Error: Attempted to use non-F opcode for F opcode instructions!")]
fn f_ops_with_non_f_opcode_panics() {
    let mut chip8 = Chip8::default();
    let instruction = Instruction::new(0x00E0);

    chip8.f_ops(&instruction);
}

#[test]
fn f_ops_set_register_from_delay_timer_is_correct() {
    // Setup a CHIP-8 state with:
    // - Delay Timer = 60
    // And set V5 to this value
    let mut chip8 = Chip8::default();
    chip8.delay_timer = 60;

    let instruction = Instruction::new(0xF507);
    chip8.f_ops(&instruction);

    assert_eq!(chip8.v[5], 60);
}

#[test]
fn f_ops_set_delay_timer_from_register_is_correct() {
    // Setup a CHIP-8 state with:
    // - V4 = 30
    let mut chip8 = Chip8::default();
    chip8.v[4] = 30;

    let instruction = Instruction::new(0xF415);
    chip8.f_ops(&instruction);

    assert_eq!(chip8.delay_timer, 30);
}

#[test]
fn f_ops_set_sound_timer_from_register_is_correct() {
    // Setup a CHIP-8 state with:
    // - V4 = 30
    let mut chip8 = Chip8::default();
    chip8.v[4] = 30;

    let instruction = Instruction::new(0xF418);
    chip8.f_ops(&instruction);

    assert_eq!(chip8.sound_timer, 30);
}

#[test]
fn f_ops_add_vx_to_i_is_correct() {
    // Setup a CHIP-8 state with:
    // - I = 0x200
    // - V2 = 0x20
    let mut chip8 = Chip8::default();
    chip8.i = 0x200;
    chip8.v[2] = 0x20;

    let instruction = Instruction::new(0xF21E);
    chip8.f_ops(&instruction);

    assert_eq!(chip8.i, 0x220);
}

#[test]
fn f_ops_get_font_sprite_location_is_correct() {
    // NOTE: This is implementation-specific, but somehow I had a bug in this code when I initially wrote it
    //       So I wrote this test to make sure that I don't break it again
    // Setup a CHIP-8 state with:
    // - V1 = 5
    let mut chip8 = Chip8::default();
    chip8.v[1] = 5;

    let instruction = Instruction::new(0xF129);
    chip8.f_ops(&instruction);

    assert_eq!(chip8.i, FONT_START as u16 + (5 * 5));
}

#[test]
fn f_ops_bcd_is_correct() {
    // Setup a CHIP-8 state with:
    // - I = 0x400
    // - V5 = 104
    let mut chip8 = Chip8::default();
    chip8.i = 0x400;
    chip8.v[5] = 104;

    let instruction = Instruction::new(0xF533);
    chip8.f_ops(&instruction);

    let i = chip8.i as usize;
    assert_eq!(chip8.ram[i], 1);
    assert_eq!(chip8.ram[i + 1], 0);
    assert_eq!(chip8.ram[i + 2], 4);
}

#[test]
fn f_ops_store_registers_in_memory_is_correct() {
    // Setup a CHIP-8 state with:
    // - I = 0x400
    // - V0 = 1
    // - V1 = 10
    // - V2 = 255
    let mut chip8 = Chip8::default();
    chip8.i = 0x400;
    chip8.v[0] = 1;
    chip8.v[1] = 10;
    chip8.v[2] = 255;

    let instruction = Instruction::new(0xF255);
    chip8.f_ops(&instruction);

    let i = chip8.i as usize;
    assert_eq!(chip8.ram[i], 1);
    assert_eq!(chip8.ram[i + 1], 10);
    assert_eq!(chip8.ram[i + 2], 255);
}

#[test]
fn f_ops_store_memory_in_registers_is_correct() {
    // Setup a CHIP-8 state with:
    // - I = 0x400
    // - RAM[I] = 1
    // - RAM[I + 1] = 10
    // - RAM[I + 2] = 255
    let mut chip8 = Chip8::default();
    chip8.i = 0x400;

    let i = chip8.i as usize;
    chip8.ram[i] = 1;
    chip8.ram[i + 1] = 10;
    chip8.ram[i + 2] = 255;

    let instruction = Instruction::new(0xF265);
    chip8.f_ops(&instruction);

    assert_eq!(chip8.v[0], 1);
    assert_eq!(chip8.v[1], 10);
    assert_eq!(chip8.v[2], 255);
}
