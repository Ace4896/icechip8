//! # GUI Module
//!
//! Contains the main application itself as well as any relevant models needed for it.
//!
//! To avoid cluttering this file too much, the views for each page are in separate modules.

mod bundled_roms;
mod emulation;
mod main_menu;
mod settings;

use std::collections::HashMap;
use std::time::Duration;

use iced::{
    button, canvas, executor,
    keyboard::{self, KeyCode},
    scrollable, slider, time, Application, Command, Container, Element, Length, Subscription,
};

use rodio::{
    source::{SineWave, Source},
    Sink,
};

use crate::chip8::{keyboard::Key, Chip8};
use crate::roms::{BundledRom, BUNDLED_ROMS};

/// The default clock speed of 500hz.
const DEFAULT_CLOCK_SPEED_HZ: u32 = 500;

lazy_static! {
    /// A hashmap containing the keymapping for Iced's keycodes to the CHIP-8 keys.
    static ref KEYMAP: HashMap<KeyCode, Key> = {
        let mut map = HashMap::new();

        map.insert(KeyCode::Key1, Key::K1);
        map.insert(KeyCode::Key2, Key::K2);
        map.insert(KeyCode::Key3, Key::K3);
        map.insert(KeyCode::Key4, Key::C);
        map.insert(KeyCode::Q, Key::K4);
        map.insert(KeyCode::W, Key::K5);
        map.insert(KeyCode::E, Key::K6);
        map.insert(KeyCode::R, Key::D);
        map.insert(KeyCode::A, Key::K7);
        map.insert(KeyCode::S, Key::K8);
        map.insert(KeyCode::D, Key::K9);
        map.insert(KeyCode::F, Key::E);
        map.insert(KeyCode::Z, Key::A);
        map.insert(KeyCode::X, Key::K0);
        map.insert(KeyCode::C, Key::B);
        map.insert(KeyCode::V, Key::F);

        map
    };
}

/// The settings for running the CHIP-8 interpreter.
#[derive(Debug, Copy, Clone)]
pub struct Chip8Settings {
    cpu_clock_speed_hz: u32,
}

impl Default for Chip8Settings {
    fn default() -> Self {
        Self {
            cpu_clock_speed_hz: DEFAULT_CLOCK_SPEED_HZ,
        }
    }
}

/// Represents the CHIP-8's state on the GUI-side.
///
/// This was used so a cache can be used for drawing the screen.
#[derive(Debug)]
pub struct Chip8State {
    chip8: Chip8,
    background_cache: canvas::Cache,
    display_cache: canvas::Cache,
}

impl Chip8State {
    pub fn with_rom(bundled_rom: &BundledRom) -> Self {
        let mut chip8 = Chip8::default();
        chip8
            .load_rom(bundled_rom.data)
            .expect("Unable to load ROM!");

        Self {
            chip8,
            background_cache: canvas::Cache::new(),
            display_cache: canvas::Cache::new(),
        }
    }
}

#[derive(Debug, Clone)]
pub enum Page {
    MainMenu,
    BundledRoms,
    Settings,
    Emulation(BundledRom),
}

/// An enum containing the models for each of the pages in this application.
/// Used in conjunction with the `Navigation(Page)` message.
enum PageModel<'a> {
    MainMenu {
        load_bundled_button: button::State,
        settings_button: button::State,
    },
    BundledRoms {
        available_roms: &'a [BundledRom],
        rom_buttons: Vec<button::State>,
        rom_button_view: scrollable::State,
        back_button: button::State,
    },
    Settings {
        cpu_clock_speed_slider: slider::State,
        back_button: button::State,
    },
    Emulation {
        state: Chip8State,
        stop_button: button::State,
    },
}

/// The main message type used for communication.
#[derive(Debug, Clone)]
pub enum Message {
    Navigation(Page),
    SettingsChanged(SettingsMessage),
    StopEmulation,
    CPUTick,
    CPUTimerTick,
    IcedEvent(iced_native::Event), // TODO: Replace this type when this is implemented on Web
}

/// The message type used for communicating changes to settings.
/// While there is only one setting at the moment, having this enum will make it easier to add new settings.
#[derive(Debug, Copy, Clone)]
pub enum SettingsMessage {
    ClockSpeed(f32),
}

/// The main Iced application itself. Stores everything that is relevant to the current state of the application.
pub struct IceCHIP8<'a> {
    settings: Chip8Settings,
    is_running: bool,
    rom_name: Option<String>,
    current_page: PageModel<'a>,
}

impl<'a> Application for IceCHIP8<'a> {
    type Executor = executor::Default;
    type Message = Message;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, Command<Self::Message>) {
        (
            Self {
                settings: Chip8Settings::default(),
                is_running: false,
                rom_name: None,
                current_page: PageModel::MainMenu {
                    load_bundled_button: button::State::new(),
                    settings_button: button::State::new(),
                },
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        if let Some(name) = &self.rom_name {
            format!("IceCHIP8 - {}", name)
        } else {
            String::from("IceCHIP8")
        }
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::Navigation(page) => {
                self.current_page = match page {
                    Page::MainMenu => PageModel::MainMenu {
                        load_bundled_button: button::State::new(),
                        settings_button: button::State::new(),
                    },
                    Page::BundledRoms => {
                        let mut rom_buttons = Vec::with_capacity(BUNDLED_ROMS.len());
                        for _ in 0..BUNDLED_ROMS.len() {
                            rom_buttons.push(button::State::new());
                        }

                        PageModel::BundledRoms {
                            rom_buttons,
                            available_roms: &BUNDLED_ROMS,
                            rom_button_view: scrollable::State::new(),
                            back_button: button::State::new(),
                        }
                    }
                    Page::Settings => PageModel::Settings {
                        cpu_clock_speed_slider: slider::State::new(),
                        back_button: button::State::new(),
                    },
                    Page::Emulation(bundled_rom) => {
                        self.is_running = true;
                        self.rom_name = Some(bundled_rom.name.to_string());

                        PageModel::Emulation {
                            state: Chip8State::with_rom(&bundled_rom),
                            stop_button: button::State::new(),
                        }
                    }
                }
            }
            Message::SettingsChanged(settings) => match settings {
                SettingsMessage::ClockSpeed(clock_speed) => {
                    self.settings.cpu_clock_speed_hz = clock_speed as u32;
                }
            },
            Message::StopEmulation => {
                self.is_running = false;
                self.rom_name = None;

                self.current_page = PageModel::MainMenu {
                    load_bundled_button: button::State::new(),
                    settings_button: button::State::new(),
                };
            }
            Message::CPUTick => {
                if let PageModel::Emulation { state, .. } = &mut self.current_page {
                    state.chip8.tick();

                    if state.chip8.is_redraw_required() {
                        // Since the cache is cleared, a redraw is forced on the next frame
                        // So we can also unset the field in the CHIP-8 state
                        state.display_cache.clear();
                        state.chip8.unset_redraw_required();
                    }
                }
            }
            Message::CPUTimerTick => {
                if let PageModel::Emulation { state, .. } = &mut self.current_page {
                    // If we need to play a sound after decrementing the sound timer, play it
                    if state.chip8.decrement_timers() {
                        // NOTE: Spawning a new thread for this is a workaround for winit and cpal (used in rodio) not playing well together
                        //       See https://github.com/RustAudio/rodio/issues/270
                        std::thread::spawn(move || {
                            let device = rodio::default_output_device()
                                .expect("Unable to obtain default audio output device!");

                            let sink = Sink::new(&device);
                            let source =
                                SineWave::new(400).take_duration(Duration::from_millis(150));
                            sink.append(source);
                            sink.play();
                            sink.sleep_until_end();
                        });
                    }
                }
            }
            Message::IcedEvent(event) => {
                if let PageModel::Emulation { state, .. } = &mut self.current_page {
                    match event {
                        iced_native::Event::Keyboard(keyboard_event) => match keyboard_event {
                            keyboard::Event::KeyPressed { key_code, .. } => {
                                if let Some(chip8_key) = KEYMAP.get(&key_code) {
                                    state.chip8.set_key_state(*chip8_key, true);
                                }
                            }
                            keyboard::Event::KeyReleased { key_code, .. } => {
                                if let Some(chip8_key) = KEYMAP.get(&key_code) {
                                    state.chip8.set_key_state(*chip8_key, false);
                                }
                            }
                            _ => (),
                        },
                        iced_native::Event::Window(window_event) => {
                            if let iced_native::window::Event::Resized { .. } = window_event {
                                state.background_cache.clear();
                                state.display_cache.clear();
                            }
                        }
                        _ => (),
                    }
                }
            }
        }

        Command::none()
    }

    fn subscription(&self) -> Subscription<Self::Message> {
        if self.is_running {
            // Subscription needed to listen to keyboard events
            let runtime_events = iced_native::subscription::events().map(Message::IcedEvent);

            // Timer for CPU ticks
            let cpu_ticks = time::every(Duration::from_millis(
                1000 / self.settings.cpu_clock_speed_hz as u64,
            ))
            .map(|_| -> Message { Message::CPUTick });

            // Timer for CPU delay and sound timer ticks
            let timer_ticks = time::every(Duration::from_millis(1000 / 60))
                .map(|_| -> Message { Message::CPUTimerTick });

            Subscription::batch(vec![runtime_events, cpu_ticks, timer_ticks])
        } else {
            Subscription::none()
        }
    }

    fn view(&mut self) -> Element<Self::Message> {
        let inner_view = match &mut self.current_page {
            PageModel::MainMenu {
                load_bundled_button,
                settings_button,
            } => main_menu::main_menu(load_bundled_button, settings_button),
            PageModel::Settings {
                cpu_clock_speed_slider,
                back_button,
            } => settings::settings(&self.settings, cpu_clock_speed_slider, back_button),
            PageModel::BundledRoms {
                available_roms,
                rom_buttons,
                rom_button_view,
                back_button,
            } => bundled_roms::bundled_roms(
                available_roms,
                rom_buttons,
                rom_button_view,
                back_button,
            ),
            PageModel::Emulation { state, stop_button } => emulation::emulation(state, stop_button),
        };

        Container::new(inner_view)
            .height(Length::Fill)
            .width(Length::Fill)
            .padding(20)
            .center_x()
            .center_y()
            .into()
    }
}
