#[macro_use]
extern crate lazy_static;

use iced::Application;

mod chip8;
mod gui;
mod roms;

fn main() {
    gui::IceCHIP8::run(iced::Settings::default());
}
